const express = require('express');
const router = express.Router();
const Posts = require('../models/post');

// Catch Errors HOF
// returns new function with chained .catch method
const catchErrors = (fn) => (req, res, next) => fn(req, res, next).catch(next);

// Create new post
const newPost = async (req, res, next) => {
  const new_post = await Posts.create(req.body);
  res.send(new_post);
};

// Retrieve all posts
const allPosts = async (req, res, next) => {
  const posts = await Posts.find({}).sort({ _id: -1 })
  res.send(posts);
};

// Retrieve specific post
const aPost = async (req, res, next) => {
  const post = await Posts.findById({ _id: req.params.id })
  res.send(post);
};

// Update specific post
const updatedPost = async (req, res, next) => {
  const update_post = await Posts.findByIdAndUpdate({ _id: req.params.id }, req.body);
  const updated_post = await Posts.findById({ _id: req.params.id });
  res.send(updated_post);
};

// Delete specific post
const deletedPost = async (req, res, next) => {
  const deleted_post = await Post.findByIdAndRemove({ _id: req.params.id });
  res.send(deleted_post);
};

// API Endpoints
router.post("/posts", catchErrors(newPost));
router.get("/posts", catchErrors(allPosts));
router.get("/posts/:id", catchErrors(aPost));
router.put("/posts/:id", catchErrors(updatedPost));
router.delete("/posts/:id", catchErrors(deletedPost));

module.exports = router;
