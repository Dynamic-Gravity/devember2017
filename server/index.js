const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const history = require('connect-history-api-fallback');

const URI = process.env.DATABASE_URL || 'mongodb://localhost/devember';
const PORT = process.env.PORT || 8081;

const api = require('./routes/api');
const app = express();

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

mongoose.connect(URI, { useMongoClient: true });
mongoose.Promise = global.Promise; // override mongoose promise because it is depricated

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function(callback) {
  console.log('Database Connected');
});

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: 'false' }));
app.use(cors());
app.use(history());
app.use('/api', api);

// better error handling middleware
const displayErrors = async (error, req, res, next) => {
  if(error.status !== 200){
    res.status(error.status || 500).send({ error: error._message });
    res.render('error', { error });
  }
}

app.use(displayErrors);

process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
});

app.listen(PORT, function() {
  console.log('Node app is running on port', PORT);
});
