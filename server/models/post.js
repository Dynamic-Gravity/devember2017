const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  createdAt: { type: Date, default: Date.now },
  title: { type: String, required: [true, 'Title is required'] },
  author: { type: String, required: [true, 'Author is required'] },
  content: { type: String, required: [true, 'Content is required'] }
});

const Post = mongoose.model('post', PostSchema);

module.exports = Post;
