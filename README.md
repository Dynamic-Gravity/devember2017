[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# Devember 2017 Challenge

This is a repo for the Devember 2017 challenge at [Level1](https://level1techs.com/).

# Contract

_I, Dynamic-Gravity, will participate to the next Devember. My Devember will be developing a REST-ful API as a MEVN Stack, and then deploy to Heroku. I promise I will program for my Devember for at least an hour, every day of the next December. I will also write a daily public devlog and will make the produced code publicly available on the internet. No matter what, I will keep my promise._

A MEVN stack refers to the development stack used:

* **M** ongo (Database)
* **E** xpress (Routing)
* **V** ue (Frontend)
* **N** odeJS (backend)

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOOiKh1Xk5RDZFKPkVXYfi8U-t2cuotiAOR7G_7w_HWXfV02TMnd9wnVM" height="50" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://i.cloudup.com/zfY6lL7eFa-3000x3000.png" height="50" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="/docs/Vue.js_Logo.svg.png" height="50" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Node.js_logo_2015.svg" height="50" />

[Heroku](https://heroku.com) is a popular PaaS for external apps. My goal is to offload the backend REST API to the Heroku instance while having the frontend work on the local system. I want to accomplish this challenge to further learn how to develop REST API's.

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

At this time, you will need to install the dependencies for both the client and server.
E.g

```bash
cd client && npm install && cd ../server && npm install
```

Once deps are installed, go to the `client/` root, `npm run dev`, and then from the `server/` root run `npm start`.

This shall run the development environments for both the Vue client and Express server.
