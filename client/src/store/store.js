import Vue from 'vue';
import Vuex from 'vuex';

import postsService from '../services/posts-service';

Vue.use(Vuex);

export const store = new Vuex.Store({
  strict: true,
  state: {
    // meta
    isLoading: false,
    hasError: false,
    // end meta
    posts: [''],
    error: ''
  },
  getters: {
    // todo
    // get posts method for clients
  },
  mutations: {
    // meta state data
    updateError: (state, payload) => { state.error = payload; },
    isLoading: (state, payload) => { state.isLoading = payload; },
    hasError: (state, payload) => { state.hasError = payload; },
    updatePosts: (state, payload) => { state.posts = payload.data; }
  },
  actions: {
    async fetchPosts({commit}) {
      commit('isLoading', true);
      try {
        const posts = await postsService.fetchPosts();
        commit('updatePosts', posts);
        commit('isLoading', false);
        commit('hasError', false );
      }
      catch (err) {
        commit('hasError', true );
        commit('updateError', err.message.toString() );
      }
    },
    async fetchPost({commit}) {
      commit('isLoading', true);
      try {
        const post = await postsService.fetchPost();
        commit('updatePost', post);
        commit('isLoading', false);
        commit('hasError', false );
      }
      catch (err) {
        commit('hasError', true );
        commit('updateError', err.message.toString() );
      }
    }
  }
});
