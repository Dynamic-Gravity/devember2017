import API from '@/services/API';

export default {
  // fetch all posts
  fetchPosts() {
    return API().get('posts');
  },
  // fetch specific post
  fetchPost(params) {
    return API().get('posts/' + params.id);
  },
  // create new post
  createPost(params) {
    return API().post('posts', params);
  },
  // update specific post
  updatePost(params) {
    return API().put('posts/' + params.id, params);
  },
  // delete specific post
  deletePost(id) {
    return API().delete('posts/' + params.id);
  }
};
