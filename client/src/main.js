// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import VeeValidate from 'vee-validate';
import VueSimpleMarkdown from 'vue-simple-markdown';
import { store } from './store/store.js';
import router from './router';

require('./assets/sass/main.scss');

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VeeValidate);
Vue.use(VueSimpleMarkdown);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
});
